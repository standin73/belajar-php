<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/dashboard', 'IndexController@dashboard');

// Route::get('/form', 'AuthController@form');

// Route::post('/utama', 'AuthController@utama');

// Route::get('/master', function(){
//     return view('layouts.master');
// });
Route::get('/pages', 'IndexController@pages');

Route::get('/table', 'IndexController@tables'); 
