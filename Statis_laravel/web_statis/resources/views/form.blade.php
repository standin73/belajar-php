<!DOCTYPE html>
<head>
    <title>Form Pendaftran</title>
</head>
<body>
    <form action="/utama" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <label for="first_name">First Name :</label><br><br>
        <input type="text" id="first_name" name="nama"><br><br>
        <label for="last_name">Last Name :</label><br><br>
        <input type="text" id="last_name" name="nama_belakang"><br><br>

        <label for="">Gender</label><br><br>
        <input type="radio" id="male" value="male" name="gender" > <label for="male">Male</label><br>
        <input type="radio" id="female" value="female" name="gender"> <label for="female">Female</label><br>
        <input type="radio" id="other" value="other"name="gender" > <label for="other">Other</label><br><br>

        <label for="nation">Nationality</label><br><br>
        <select name="nationality" id="nation">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select> <br><br>

        <label for="">Language Spoken</label><br><br>
        <input type="checkbox" id="indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="inggris">
        <label for="inggris">Inggris</label><br>
        <input type="checkbox" id="other">
        <label for="other">Other</label><br><br>

        <label for="bio">Bio</label><br>
        <textarea id="bio">

        </textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>