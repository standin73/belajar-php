<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }
    public function utama(Request $request){
        $nama = $request['nama'];
        $nama_belakang = $request['nama_belakang'];
        return view('utama', compact('nama', 'nama_belakang'));
    }
}
