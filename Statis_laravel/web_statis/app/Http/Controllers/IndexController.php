<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function dashboard(){
        return view('dashboard');
    }
    public function pages(){
        return view('pages.dashboard');
    }
    public function tables(){
        return view('pages.datatables');
    }
}
