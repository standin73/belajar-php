<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    require_once ('animal.php'); 
    require_once ('frog.php');
    require_once ('ape.php');
    
    $sheep = new Animal("shaun");
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cool blooded : " . $sheep->cold_blooded . "<br>". "<br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "Legs : " . $sungokong->legs . "<br>";
    echo "Cool blooded : " . $sungokong->cold_blooded . "<br>"; 
    echo "Yell : " . $sungokong->yell() . "<br>" . "<br>" ; // "Auooo"

    $kodok = new Frog("buduk");
    echo "Legs : " . $kodok->legs . "<br>";
    echo "Cool blooded : " . $kodok->cold_blooded . "<br>"; 
    echo "jump" . $kodok->jump() ; // "hop hop"
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    
    ?>
</body>
</html>