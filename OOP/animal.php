<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 

    class Animal{
        public $legs = 4;
        public $cold_blooded = "no";
        public $name;
        public function __construct($name){
            echo "Name : $name  <br>";
        }
        // public function printName($name){
        //     echo  "Name : $name";
        // }

    }

    // class Ape extends Animal {
    //     public $legs = 2;
    //     public function yell(){
    //         echo 'Auoo';
    //     }
    // }

    // class Frog extends Animal{
    //     public function jump(){
    //         echo "hop hop";
    //     }
    // }
    ?>
</body>
</html>